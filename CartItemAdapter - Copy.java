package com.example.sevenstore.cart;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.sevenstore.R;
import com.example.sevenstore.model.CartItem;
import com.example.sevenstore.model.CartModel;
import com.example.sevenstore.model.Product;
import com.example.sevenstore.utils.PriceConverter;
import com.example.sevenstore.view.CartItemCountChanger;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CartItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
  private CartModel cartModel;
  private CartItemEventListener eventListener;

  public CartItemAdapter(CartModel cartModel, CartItemEventListener eventListener) {
    this.cartModel = cartModel;
    this.eventListener = eventListener;
  }

  public void updateCartModel(CartModel cartModel) {
    this.cartModel = cartModel;
    notifyDataSetChanged();
  }

  public void notifyItemChanged(CartItem cartItem) {
    int index = cartModel.getCartItems().indexOf(cartItem);
    notifyItemChanged(index);
  }

  public void removeCartItem(CartItem cartItem) {
    int index = cartModel.getCartItems().indexOf(cartItem);
    cartModel.getCartItems().remove(index);
    notifyItemRemoved(index);
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    return new CartItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(
      R.layout.item_cart, parent, false
    ));

  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    if (holder instanceof CartItemViewHolder) {
      ((CartItemViewHolder) holder).bindCartItem(cartModel.getCartItems().get(position));

    }
  }

  @Override
  public int getItemCount() {
    return cartModel.getCartItems().size();
  }

  public class CartItemViewHolder extends RecyclerView.ViewHolder {
    private TextView titleTv;
    private ImageView productImage;
    private CartItemCountChanger cartItemCountChanger;
    private TextView removeBtn;
    private ProgressBar removeProgressBar;
    private ProgressBar changeCountProgressBar;
    private TextView prevPriceTv;
    private TextView priceTv;

    public CartItemViewHolder(@NonNull View itemView) {
      super(itemView);
      titleTv = itemView.findViewById(R.id.tv_cartItem_productTitle);
      productImage = itemView.findViewById(R.id.iv_cartItem_productImage);
      cartItemCountChanger = itemView.findViewById(R.id.cartItemCountChanger_productItemCount);
      removeBtn = itemView.findViewById(R.id.iv_cartItem_remove);
      removeProgressBar = itemView.findViewById(R.id.prg_cartItem_remove);
      changeCountProgressBar = itemView.findViewById(R.id.prg_cartItem_changeCount);
      priceTv = itemView.findViewById(R.id.tv_cartItem_currentPrice);
      prevPriceTv = itemView.findViewById(R.id.tv_cartItem_prevPrice);

    }

    public void bindCartItem(CartItem cartItem) {
      titleTv.setText(cartItem.getProduct().getTitle());
      Picasso.get().load(cartItem.getProduct().getImage()).into(productImage);
      priceTv.setText(PriceConverter.convert(cartItem.getProduct().getPrice()));
      prevPriceTv.setText(PriceConverter.convert(cartItem.getProduct().getPreviousPrice()));
      cartItemCountChanger.setCount(cartItem.getCount());

      if (cartItem.isRemoving()) {
        removeBtn.setVisibility(View.INVISIBLE);
        removeProgressBar.setVisibility(View.VISIBLE);
      } else {
        removeBtn.setVisibility(View.VISIBLE);
        removeProgressBar.setVisibility(View.GONE);
      }

      if (cartItem.isChangingCount()) {
        cartItemCountChanger.setVisibility(View.INVISIBLE);
        changeCountProgressBar.setVisibility(View.VISIBLE);
      } else {
        cartItemCountChanger.setVisibility(View.VISIBLE);
        changeCountProgressBar.setVisibility(View.GONE);
      }

      removeBtn.setOnClickListener(v -> {
        cartItem.setRemoving(true);
        eventListener.onRemoveButtonClick(cartItem);
        notifyItemChanged(getAdapterPosition());
      });

      cartItemCountChanger.setCartItemCountChange(count -> {
        eventListener.onCartItemCountChange(cartItem, count);
        cartItem.setChangingCount(true);
        notifyItemChanged(getAdapterPosition());
      });

      productImage.setOnClickListener(v -> eventListener.onProductClick(cartItem.getProduct()));
    }

  }

  public interface CartItemEventListener {
    void onProductClick(Product product);

    void onRemoveButtonClick(CartItem cartItem);

    void onCartItemCountChange(CartItem cartItem, int requestedCount);
  }

}
