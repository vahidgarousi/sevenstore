package com.example.sevenstore.shipping

import com.example.sevenstore.model.*
import com.example.sevenstore.providers.*
import com.google.gson.*
import io.reactivex.*
import io.reactivex.subjects.*

class ShippingViewModel {
    private var apiService = ApiServiceProvider.provideApiService()
    private var progressBarVisibilityBehaviorSubject: BehaviorSubject<Boolean> =
        BehaviorSubject.create()

    fun orderSubmit(
        firstName: String,
        lastName: String,
        phoneNumber: String,
        postalCode: String,
        address: String,
        paymentMethod: PaymentMethod
    ): Single<OrderSubmitResponse> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("first_name", firstName)
        jsonObject.addProperty("last_name", lastName)
        jsonObject.addProperty("mobile", phoneNumber)
        jsonObject.addProperty("postal_code", postalCode)
        jsonObject.addProperty("address", address)
        jsonObject.addProperty("payment_method", paymentMethod.ordinal)
        progressBarVisibilityBehaviorSubject.onNext(true)
        return apiService.submitOrder(jsonObject).doOnEvent { t1, t2 ->
            progressBarVisibilityBehaviorSubject.onNext(false)
        }
    }

    fun getProgressBarVisibility(): BehaviorSubject<Boolean> {
        return progressBarVisibilityBehaviorSubject
    }

}
