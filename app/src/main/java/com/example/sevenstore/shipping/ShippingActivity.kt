package com.example.sevenstore.shipping

import android.content.*
import android.net.*
import android.os.*
import android.view.*
import com.example.sevenstore.*
import com.example.sevenstore.cart.*
import com.example.sevenstore.checkout.*
import com.example.sevenstore.model.*
import com.example.sevenstore.model.api.*
import io.reactivex.android.schedulers.*
import io.reactivex.disposables.*
import io.reactivex.schedulers.*
import kotlinx.android.synthetic.main.activity_shipping.*

class ShippingActivity : BaseActivity() {
    var shippingViewModel = ShippingViewModel()
    var compositeDisposable = CompositeDisposable()
    var payable: Long = -1
    var totalPrice: Long = -1
    var shippingCost: Long = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shipping)
        payable = intent!!.getLongExtra(EXTRA_KEY_PAYABLE, -1)
        totalPrice = intent!!.getLongExtra(EXTRA_KEY_TOTAL_PRICE, -1)
        shippingCost = intent!!.getLongExtra(EXTRA_KEY_SHIPPING_COST, -1)
        if (payable.equals(-1) || totalPrice.equals(-1) || shippingCost.equals(-1)) {
            finish()
        }
        setupViews()
        observe()
    }

    private fun observe() {
        compositeDisposable.add(shippingViewModel.getProgressBarVisibility()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                // prg_details_comment.visibility = if (it) View.VISIBLE else View.GONE
                ln_shipping_paymentButtonContainer.visibility = if (it) View.GONE else View.VISIBLE
            })
    }

    private fun setupViews() {
        btn_shipping_onlinePayment.setOnClickListener {
            submitOrder(PaymentMethod.ONLINE_METHOD)
        }
        btn_shipping_cacheOnDelivery.setOnClickListener {
            submitOrder(PaymentMethod.CACHE_ON_DELIVERY)

        }
        val view = findViewById<View>(R.id.layout_shipping_purchaseDetail)
        val viewHolder = CartItemAdapter.PurchaseDetailViewHolder(view)
        viewHolder.bindDetails(totalPrice, shippingCost, payable)

        iv_shipping_back.setOnClickListener {
            finish()
        }
    }

    override fun getCoordinatorLayoutId(): Int {
        return R.id.cl_shipping
    }

    private fun submitOrder(paymentMethod: PaymentMethod) {
        // ToDo You must validate fields
        shippingViewModel.orderSubmit(
            edt_shipping_firstName.text.toString(),
            edt_shipping_lastName.text.toString(),
            edt_shipping_phoneNumber.text.toString(),
            edt_shipping_postalCode.text.toString(),
            edt_shipping_address.text.toString(),
            paymentMethod
        ).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleSevenStoreObserver<OrderSubmitResponse>(this) {
                override fun onSuccess(orderResponse: OrderSubmitResponse) {
                    when (paymentMethod) {
                        PaymentMethod.CACHE_ON_DELIVERY -> {
                            val intent = Intent(this@ShippingActivity, CheckoutActivity::class.java)
                            intent.putExtra("order_id", orderResponse.orderId.toString())
                            startActivity(intent)
                        }
                        PaymentMethod.ONLINE_METHOD -> {
                            val bankGateWay = Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("http://expertdevelopers.ir/payment?order_id=" + orderResponse.orderId)
                            )
                            startActivity(bankGateWay)
                        }
                    }
                    finish()
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })
    }

    companion object {
        const val EXTRA_KEY_PAYABLE = "payable"
        const val EXTRA_KEY_TOTAL_PRICE = "total"
        const val EXTRA_KEY_SHIPPING_COST = "shipping_cost"
    }
}
