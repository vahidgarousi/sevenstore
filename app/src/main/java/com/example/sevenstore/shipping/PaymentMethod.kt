package com.example.sevenstore.shipping

enum class PaymentMethod(mode: String) {
    ONLINE_METHOD("online"), CACHE_ON_DELIVERY("cache_on_delivery")

}