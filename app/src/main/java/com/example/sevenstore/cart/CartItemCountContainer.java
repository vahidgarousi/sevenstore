package com.example.sevenstore.cart;

public class CartItemCountContainer {
  public static int count;

  public static void updateCount(int count) {
    CartItemCountContainer.count = count;
  }

  public static int getCount() {
    return count;
  }
}
