//package com.example.sevenstore.cart
//
//import android.bluetooth.le.*
//import android.graphics.*
//import android.view.*
//import android.widget.*
//import androidx.appcompat.widget.*
//import androidx.recyclerview.widget.*
//import com.example.sevenstore.R
//import com.example.sevenstore.model.*
//import com.example.sevenstore.utils.*
//import com.example.sevenstore.view.*
//import com.squareup.picasso.*
//
//class CartItemAdapter(var cartModel: CartModel, var cartItemEventListener: CartItemEventListener) :
//    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
//
//    init {
//        eventListener = cartItemEventListener
//        mCartModel = cartModel
//    }
//
//    fun updateCartModel(cartModel: CartModel) {
//        mCartModel = cartModel
//        notifyDataSetChanged()
//    }
//
//    fun removeCartItem(cartItem: CartItem) {
//        val index = mCartModel!!.cartItems.indexOf(cartItem)
//        mCartModel!!.cartItems.removeAt(index)
//        notifyItemRemoved(index)
//    }
//
//    fun notifyItemChanged(cartItem: CartItem) {
//        val index = mCartModel!!.cartItems.indexOf(cartItem)
//        notifyItemChanged(index)
//    }
//
//    companion object {
//        var eventListener: CartItemEventListener? = null
//        var mCartModel: CartModel? = null
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
//        return CartItemViewHolder(
//            LayoutInflater.from(parent.context).inflate(
//                R.layout.item_cart,
//                parent,
//                false
//            )
//        )
//    }
//
//    override fun getItemCount(): Int {
//        return cartModel.cartItems.size
//    }
//
//    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        if (holder is CartItemViewHolder) {
//            holder.bindCart(cartModel.cartItems[position])
//            holder.cartItemRemoveTV.setOnClickListener {
//                eventListener!!.onRemoveButtonClicked(cartItem)
//                cartItem.isRemoving = true
//            }
//        }
//    }
//
//
//    class CartItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//        var cartItemImage = itemView.findViewById<AppCompatImageView>(R.id.iv_cartItem_productImage)
//        var cartItemTitle = itemView.findViewById<TextView>(R.id.tv_cartItem_productTitle)
//        var cartItemCPrice = itemView.findViewById<TextView>(R.id.tv_cartItem_currentPrice)
//        var cartItemRemoveTV = itemView.findViewById<TextView>(R.id.iv_cartItem_remove)
//        var cartItemRemoveProgressBar = itemView.findViewById<ProgressBar>(R.id.prg_cartItem_remove)
//        var cartItemPPrice = itemView.findViewById<TextView>(R.id.tv_cartItem_prevPrice)
//        var cartItemCountChanger =
//            itemView.findViewById<CartItemCountChanger>(R.id.cartItemCountChanger_productItemCount)
//        var cartItemCountChangerProgressBar =
//            itemView.findViewById<ProgressBar>(R.id.prg_cartItem_changeCount)
//
//        fun bindCart(cartItem: CartItem) {
//            Picasso.get().load(cartItem.product.image).into(cartItemImage)
//            cartItemTitle.text = cartItem.product.title
//            cartItemCPrice.text = PriceConverter.convert(cartItem.product.price)
//            cartItemPPrice.paintFlags = cartItemPPrice.paintFlags.or(Paint.STRIKE_THRU_TEXT_FLAG)
//            cartItemPPrice.text = PriceConverter.convert(cartItem.product.previousPrice)
//
//            if (cartItem.isRemoving) {
//                cartItemRemoveTV.visibility = View.INVISIBLE
//                cartItemRemoveProgressBar.visibility = View.VISIBLE
//            } else {
//                cartItemRemoveTV.visibility = View.VISIBLE
//                cartItemRemoveProgressBar.visibility = View.INVISIBLE
//            }
//
//            cartItemRemoveTV.setOnClickListener {
//                eventListener!!.onRemoveButtonClicked(cartItem)
//                cartItem.isRemoving = true
//            }
//
//            if (cartItem.isChangingCount) {
//                cartItemCountChanger.visibility = View.GONE
//                cartItemCountChangerProgressBar.visibility = View.VISIBLE
//            } else {
//                cartItemCountChanger.visibility = View.VISIBLE
//                cartItemCountChangerProgressBar.visibility = View.GONE
//            }
//
//            cartItemCountChanger.setCartItemCountChange(object :
//                CartItemCountChanger.OnCartItemCountChangeListener {
//                override fun onChanged(count: Int) {
//                    eventListener!!.onCartItemCountChanged(cartItem, count)
//                }
//            })
//
//            cartItemImage.setOnClickListener {
//                eventListener!!.onProductClicked(cartItem.product)
//            }
//        }
//    }
//
//    interface CartItemEventListener {
//        fun onProductClicked(product: Product)
//        fun onRemoveButtonClicked(cartItem: CartItem)
//        fun onCartItemCountChanged(cartItem: CartItem, requestedCount: Int)
//
//    }
//}