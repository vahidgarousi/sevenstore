package com.example.sevenstore.cart;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.sevenstore.R;
import com.example.sevenstore.model.CartItem;
import com.example.sevenstore.model.CartModel;
import com.example.sevenstore.model.Product;
import com.example.sevenstore.utils.PriceConverter;
import com.example.sevenstore.view.CartItemCountChanger;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CartItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private static final int TYPE_CART_ITEM = 0;
  private static final int TYPE_PURCHASE_DETAIL = 1;

  private CartModel cartModel;
  private CartItemEventListener eventListener;

  public CartItemAdapter(CartModel cartModel, CartItemEventListener eventListener) {
    this.cartModel = cartModel;
    this.eventListener = eventListener;
  }

  public void updateCartModel(CartModel cartModel) {
    this.cartModel = cartModel;
    notifyDataSetChanged();
  }

  public void notifyItemChanged(CartItem cartItem) {
    int index = cartModel.getCartItems().indexOf(cartItem);
    notifyItemChanged(index);
  }

  public void removeCartItem(CartItem cartItem) {
    int index = cartModel.getCartItems().indexOf(cartItem);
    cartModel.getCartItems().remove(index);
    notifyItemRemoved(index);
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    if (viewType == TYPE_CART_ITEM) {
      return new CartItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(
        R.layout.item_cart, parent, false
      ));
    } else {
      return new PurchaseDetailViewHolder(LayoutInflater.from(parent.getContext()).inflate(
        R.layout.item_purchase_details, parent, false
      ));
    }
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    if (holder instanceof CartItemViewHolder) {
      ((CartItemViewHolder) holder).bindCartItem(cartModel.getCartItems().get(position));
    } else if (holder instanceof PurchaseDetailViewHolder) {
      ((PurchaseDetailViewHolder) holder).bindDetails(cartModel.getTotalPrice(), cartModel.getShippingCost(), cartModel.getPayablePrice());
    }
  }

  @Override
  public int getItemCount() {
    return cartModel.getCartItems().size() + 1;
  }


  @Override
  public int getItemViewType(int position) {
    if (position < cartModel.getCartItems().size()) {
      return TYPE_CART_ITEM;
    }
    return TYPE_PURCHASE_DETAIL;

  }

  public interface CartItemEventListener {
    void onProductClick(Product product);

    void onRemoveButtonClick(CartItem cartItem);

    void onCartItemCountChange(CartItem cartItem, int requestedCount);
  }

  public class CartItemViewHolder extends RecyclerView.ViewHolder {
    private TextView titleTv;
    private ImageView productImage;
    private CartItemCountChanger cartItemCountChanger;
    private TextView removeBtn;
    private ProgressBar removeProgressBar;
    private ProgressBar changeCountProgressBar;
    private TextView prevPriceTv;
    private TextView priceTv;

    public CartItemViewHolder(@NonNull View itemView) {
      super(itemView);
      titleTv = itemView.findViewById(R.id.tv_cartItem_productTitle);
      productImage = itemView.findViewById(R.id.iv_cartItem_productImage);
      cartItemCountChanger = itemView.findViewById(R.id.cartItemCountChanger_productItemCount);
      removeBtn = itemView.findViewById(R.id.iv_cartItem_remove);
      removeProgressBar = itemView.findViewById(R.id.prg_cartItem_remove);
      changeCountProgressBar = itemView.findViewById(R.id.prg_cartItem_changeCount);
      priceTv = itemView.findViewById(R.id.tv_cartItem_currentPrice);
      prevPriceTv = itemView.findViewById(R.id.tv_cartItem_prevPrice);

    }

    public void bindCartItem(CartItem cartItem) {
      titleTv.setText(cartItem.getProduct().getTitle());
      String image = cartItem.getProduct().getImage().replace("http://expertdevelopers.ir/storage/", "");
      Picasso.get().load(image).into(productImage);
      priceTv.setText(PriceConverter.convert(cartItem.getProduct().getPrice()));
      prevPriceTv.setPaintFlags(prevPriceTv.getPaintFlags() | (Paint.STRIKE_THRU_TEXT_FLAG));

      prevPriceTv.setText(PriceConverter.convert(cartItem.getProduct().getPreviousPrice()));
      cartItemCountChanger.setCount(cartItem.getCount());

      if (cartItem.isRemoving()) {
        removeBtn.setVisibility(View.INVISIBLE);
        removeProgressBar.setVisibility(View.VISIBLE);
      } else {
        removeBtn.setVisibility(View.VISIBLE);
        removeProgressBar.setVisibility(View.GONE);
      }

      if (cartItem.isChangingCount()) {
        cartItemCountChanger.setVisibility(View.INVISIBLE);
        changeCountProgressBar.setVisibility(View.VISIBLE);
      } else {
        cartItemCountChanger.setVisibility(View.VISIBLE);
        changeCountProgressBar.setVisibility(View.INVISIBLE);
      }

      removeBtn.setOnClickListener(v -> {
        cartItem.setRemoving(true);
        eventListener.onRemoveButtonClick(cartItem);
        notifyItemChanged(getAdapterPosition());
      });

      cartItemCountChanger.setCartItemCountChange(count -> {
        eventListener.onCartItemCountChange(cartItem, count);
        cartItem.setChangingCount(true);
        notifyItemChanged(getAdapterPosition());
      });

      productImage.setOnClickListener(v -> eventListener.onProductClick(cartItem.getProduct()));
    }

  }

  public static class PurchaseDetailViewHolder extends RecyclerView.ViewHolder {
    private TextView totalPriceTv;
    private TextView payablePriceTv;
    private TextView shippingCostTv;

    public PurchaseDetailViewHolder(@NonNull View itemView) {
      super(itemView);
      totalPriceTv = itemView.findViewById(R.id.tv_purchaseDetail_totalPrice);
      payablePriceTv = itemView.findViewById(R.id.tv_purchaseDetail_payable);
      shippingCostTv = itemView.findViewById(R.id.tv_purchaseDetail_shippingCost);

    }

    public void bindDetails(long totalPrice, long shippingCost, long payablePrice) {
      totalPriceTv.setText(PriceConverter.convert(totalPrice));
      if (shippingCost > 0)
        shippingCostTv.setText(PriceConverter.convert(shippingCost));
      else
        shippingCostTv.setText("رایگان");
      payablePriceTv.setText(PriceConverter.convert(payablePrice));
    }

  }

}
