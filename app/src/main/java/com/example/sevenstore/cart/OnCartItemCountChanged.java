package com.example.sevenstore.cart;

public class OnCartItemCountChanged {
  private int count;

  public OnCartItemCountChanged(int count) {
    this.count = count;
  }

  public int getCount() {
    return count;
  }
}
