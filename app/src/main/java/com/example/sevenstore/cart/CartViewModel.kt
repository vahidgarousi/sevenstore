package com.example.sevenstore.cart

import com.example.sevenstore.model.*
import com.example.sevenstore.providers.*
import com.google.gson.*
import io.reactivex.*
import io.reactivex.subjects.*

class CartViewModel {
    private var apiService = ApiServiceProvider.provideApiService()
    private var progressBarVisibilityBehaviorSubject: BehaviorSubject<Boolean> =
        BehaviorSubject.create()

    fun getProgressBarVisibility(): BehaviorSubject<Boolean> {
        return progressBarVisibilityBehaviorSubject
    }

    fun cart(mustShowProgressBar: Boolean): Single<CartModel> {
        if (mustShowProgressBar)
            progressBarVisibilityBehaviorSubject.onNext(true)
        return apiService.cart.doOnEvent { t1, t2 ->
            progressBarVisibilityBehaviorSubject.onNext(false)
        }
    }

    fun addCartItem(jsonObject: JsonObject): Single<AddToCartResponse> {
        return apiService.addToCart(jsonObject)
    }

    fun removeCartItem(cartItem: CartItem): Single<SuccessResponse> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("cart_item_id", cartItem.cartItemId)
        return apiService.removeCartItem(jsonObject)
    }

    fun changeCartItem(cartItem: CartItem, newCount: Int): Single<AddToCartResponse> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("cart_item_id", cartItem.cartItemId)
        jsonObject.addProperty("count", newCount)
        return apiService.changeCartItemCount(jsonObject)
    }
}