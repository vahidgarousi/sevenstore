package com.example.sevenstore.cart

import android.content.*
import android.os.*
import android.view.*
import androidx.recyclerview.widget.*
import com.example.sevenstore.*
import com.example.sevenstore.details.*
import com.example.sevenstore.model.*
import com.example.sevenstore.model.api.*
import com.example.sevenstore.shipping.*
import io.reactivex.android.schedulers.*
import io.reactivex.disposables.*
import io.reactivex.schedulers.*
import kotlinx.android.synthetic.main.activity_cart.*
import org.greenrobot.eventbus.*

class CartActivity : BaseActivity(), CartItemAdapter.CartItemEventListener {
    var compositeDisposable = CompositeDisposable()
    var cartViewModel = CartViewModel()
    var cartItemAdapter: CartItemAdapter? = null
    override fun onProductClick(product: Product?) {
        val intent = Intent(this@CartActivity, ProductDetailsActivity::class.java)
        intent.putExtra(ProductDetailsActivity.EXTRA_KEY_PRODUCT, product)
        startActivity(intent)
    }

    override fun onRemoveButtonClick(cartItem: CartItem?) {
        cartViewModel.removeCartItem(cartItem!!).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleSevenStoreObserver<SuccessResponse>(this) {
                override fun onSuccess(t: SuccessResponse) {
                    cartItemAdapter!!.removeCartItem(cartItem)
                    getCartItems(false)
                    var count = CartItemCountContainer.getCount()
                    count -= 1
                    CartItemCountContainer.updateCount(count)
                    EventBus.getDefault().post(object : OnCartItemCountChanged(count) {})
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

                override fun onError(e: Throwable) {
                    super.onError(e)
                    cartItem.isRemoving = false
                    cartItemAdapter!!.notifyItemChanged(cartItem)
                    getCartItems(false)
                }
            })
    }

    override fun onCartItemCountChange(cartItem: CartItem?, requestedCount: Int) {
        cartViewModel.changeCartItem(cartItem!!, requestedCount)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleSevenStoreObserver<AddToCartResponse>(this@CartActivity) {
                override fun onSuccess(t: AddToCartResponse) {
                    cartItem.count = requestedCount
                    cartItem.isChangingCount = false
                    cartItemAdapter!!.notifyItemChanged(cartItem)
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

                override fun onError(e: Throwable) {
                    super.onError(e)
                    cartItem.isChangingCount = false
                    cartItemAdapter!!.notifyItemChanged(cartItem)
                }

            })
    }


    private fun getCartItems(mustShowProgressBar: Boolean) {
        cartViewModel.cart(mustShowProgressBar).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleSevenStoreObserver<CartModel>(this) {
                override fun onSuccess(cartModel: CartModel) {
                    btn_cart_submit.setOnClickListener {
                        val intent = Intent(this@CartActivity, ShippingActivity::class.java)
                        intent.putExtra(ShippingActivity.EXTRA_KEY_PAYABLE, cartModel.payablePrice)
                        intent.putExtra(
                            ShippingActivity.EXTRA_KEY_SHIPPING_COST,
                            cartModel.shippingCost
                        )
                        intent.putExtra(
                            ShippingActivity.EXTRA_KEY_TOTAL_PRICE,
                            cartModel.totalPrice
                        )
                        startActivity(intent)
                        finish()
                    }
                    CartItemCountContainer.updateCount(cartModel.cartItems.size)
                    EventBus.getDefault()
                        .post(object : OnCartItemCountChanged(cartModel.cartItems.size) {

                        })
                    if (cartModel.cartItems.isEmpty()) {
                        fl_cart_emptyState.visibility = View.VISIBLE
                    } else {
                        fl_cart_emptyState.visibility = View.GONE
                    }
                    if (cartItemAdapter == null) {
                        cartItemAdapter = CartItemAdapter(cartModel, this@CartActivity)
                        rv_cart.layoutManager = LinearLayoutManager(
                            this@CartActivity,
                            LinearLayoutManager.VERTICAL,
                            false
                        )
                        rv_cart.adapter = cartItemAdapter
                    }
                    cartItemAdapter!!.updateCartModel(cartModel)
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })
    }


    override fun getCoordinatorLayoutId(): Int {
        return R.id.cl_cart
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
        setupViews()
        getCartItems(true)

        compositeDisposable.add(cartViewModel.getProgressBarVisibility().subscribeOn(
            AndroidSchedulers.mainThread()
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                fl_cart_progressBar.visibility = if (it) View.VISIBLE else View.INVISIBLE
            })

    }

    private fun setupViews() {
        tv_cart_viewPurchaseDetail.setOnClickListener {
            rv_cart.smoothScrollToPosition(cartItemAdapter!!.itemCount)
        }
        iv_cart_back.setOnClickListener {
            finish()
        }
        btn_cart_back.setOnClickListener {
            finish()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    override fun onResume() {
        super.onResume()
        updateCartItemCountBadge(CartItemCountContainer.getCount())
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onCartItemCountChanged(onCartItemCountChanged: OnCartItemCountChanged) {
        updateCartItemCountBadge(onCartItemCountChanged.count)
    }

    private fun updateCartItemCountBadge(count: Int) {
        if (count > 0) {
            tv_cart_cartItemCountBadge.visibility = View.VISIBLE
            tv_cart_cartItemCountBadge.text = count.toString()

        } else {
            tv_cart_cartItemCountBadge.visibility = View.GONE
        }
    }
}
