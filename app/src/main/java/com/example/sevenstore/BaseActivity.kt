package com.example.sevenstore

import android.content.*
import android.os.*
import androidx.appcompat.app.*
import com.example.sevenstore.auth.*
import com.example.sevenstore.exception.*
import com.google.android.material.snackbar.*
import org.greenrobot.eventbus.*

abstract class BaseActivity : AppCompatActivity() {
    abstract fun getCoordinatorLayoutId(): Int
    fun showSnackBarMessage(message: String) {
        Snackbar.make(findViewById(getCoordinatorLayoutId()), message, Snackbar.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventBus.getDefault().register(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }


    @Subscribe
    fun handleUnAuthorizationException(unAuthorizationException: UnAuthorizationException) {
        startActivity(Intent(this@BaseActivity, AuthenticationActivity::class.java))
    }
}