package com.example.sevenstore;

import android.app.Application;

import com.example.sevenstore.auth.TokenContainer;
import com.example.sevenstore.auth.UserInfoManager;

public class App extends Application {
  @Override
  public void onCreate() {
    super.onCreate();
    TokenContainer.updateToken(new UserInfoManager(this).getToken());
  }
}
