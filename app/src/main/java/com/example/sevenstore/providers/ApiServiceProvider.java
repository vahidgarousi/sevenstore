package com.example.sevenstore.providers;

import com.example.sevenstore.model.api.ApiService;
import com.example.sevenstore.model.api.RetrofitSingleton;

public class ApiServiceProvider {

    private static ApiService apiService;

    public static ApiService provideApiService() {
        if (apiService == null) {
            apiService = RetrofitSingleton.getInstance().create(ApiService.class);
        }
        return apiService;
    }
}
