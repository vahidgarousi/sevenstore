package com.example.sevenstore.checkout

import com.example.sevenstore.model.*
import com.example.sevenstore.providers.*
import io.reactivex.*
import io.reactivex.subjects.*

class CheckoutViewModel {
    private val apiService = ApiServiceProvider.provideApiService()
    val progressBarVisibilitySubject = BehaviorSubject.create<Boolean>()
    fun getProgressBarVisibility(): BehaviorSubject<Boolean> {
        return progressBarVisibilitySubject
    }

    fun checkout(orderId: String): Single<CheckoutResponse> {
        progressBarVisibilitySubject.onNext(true)
        return apiService.checkout(orderId).doOnEvent { t1, t2 ->
            progressBarVisibilitySubject.onNext(false)
        }
    }

}