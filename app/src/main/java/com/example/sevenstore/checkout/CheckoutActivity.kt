package com.example.sevenstore.checkout

import android.os.*
import android.view.*
import com.example.sevenstore.*
import com.example.sevenstore.model.*
import com.example.sevenstore.model.api.*
import com.example.sevenstore.utils.*
import io.reactivex.android.schedulers.*
import io.reactivex.disposables.*
import io.reactivex.schedulers.*
import kotlinx.android.synthetic.main.activity_checkuot.*

class CheckoutActivity : BaseActivity() {
    override fun getCoordinatorLayoutId(): Int {
        return R.id.cl_checkout
    }

    var compositeDisposable = CompositeDisposable()
    var checkoutViewModel = CheckoutViewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkuot)
        val uri = intent.data
        var orderId = ""
        if (uri != null) {
            orderId = uri.getQueryParameter("order_id")!!
        } else {
            orderId = intent.getStringExtra("order_id")
        }
        checkoutViewModel.checkout(orderId)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleSevenStoreObserver<CheckoutResponse>(this) {
                override fun onSuccess(t: CheckoutResponse) {
                    showData(t)
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })

        compositeDisposable.add(checkoutViewModel.getProgressBarVisibility()
            .subscribeOn(AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                prg_checkout_main.visibility = if (it) View.VISIBLE else View.GONE
            })

    }

    fun showData(checkoutResponse: CheckoutResponse) {
        tv_checkout_message.text =
            if (checkoutResponse.purchaseSuccess) "خرید با موفقیت انجام شد" else "فرآیند خرید با شکست مواجه شد"
        tv_checkout_orderStatus.text = checkoutResponse.paymentStatus
        tv_checkout_orderPayPrice.text = PriceConverter.convert(checkoutResponse.payablePrice)
        btn_checkout_backToHome.setOnClickListener {
            finish()
        }
        btn_checkout_orderHistory.setOnClickListener {

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}
