package com.example.sevenstore.view

import android.content.*
import android.util.*
import android.view.*
import android.widget.*
import androidx.cardview.widget.*
import com.example.sevenstore.*

class CartItemCountChanger : CardView {

    private var mRootView: View? = null
    private lateinit var add: TextView
    private lateinit var minus: TextView
    private lateinit var countTv: TextView
    private var onCArtItemCountChangeListener: OnCartItemCountChangeListener? = null

    fun setCartItemCountChange(onCArtItemCountChangeListener: OnCartItemCountChangeListener) {
        this.onCArtItemCountChangeListener = onCArtItemCountChangeListener
    }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    private fun init() {
        mRootView =
            LayoutInflater.from(context).inflate(R.layout.view_cart_item_count_changer, this, true)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        add = mRootView!!.findViewById(R.id.tv_cartItemCountChanger_add)
        minus = mRootView!!.findViewById(R.id.tv_cartItemCountChanger_minus)
        countTv = mRootView!!.findViewById(R.id.tv_cartItemCountChanger_count)
        add.setOnClickListener {
            count = count.plus(1)
            onCountChanged()
        }
        minus.setOnClickListener {
            count = count.minus(1)
            onCountChanged()
        }


    }


    private fun onCountChanged() {
        if (count >= 0) {
            countTv.text = count.toString()
            onCArtItemCountChangeListener?.onChanged(count)
        } else {
            count = 1
        }
    }

    interface OnCartItemCountChangeListener {
        fun onChanged(count: Int)
    }

    private var count: Int = 0

    fun setCount(count: Int) {
        this.count = count
        countTv.text = count.toString()
    }

    fun getCount(): Int {
        return count
    }

}
