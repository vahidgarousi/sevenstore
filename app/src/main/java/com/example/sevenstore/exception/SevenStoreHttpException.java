
package com.example.sevenstore.exception;

import com.google.gson.annotations.Expose;


public class SevenStoreHttpException {

    @Expose
    private String error;
    @Expose
    private String message;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
