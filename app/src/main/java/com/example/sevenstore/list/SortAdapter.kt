package com.example.sevenstore.list

import android.content.*
import android.view.*
import android.widget.*
import androidx.core.content.*
import androidx.recyclerview.widget.*
import com.example.sevenstore.*

class SortAdapter(
    var context: Context,
    private var selectedSortType: Int,
    var onSortClickListener: OnSortClickListener
) :
    RecyclerView.Adapter<SortAdapter.SortViewHolder>() {
    private var sortTypeIds = arrayOf(
        "جدیدترین",
        "پربازدیدترین",
        "قیمت از کم به زیاد",
        "قیمت از زیاد به کم"
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SortViewHolder {
        return SortViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_sort_chips,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return sortTypeIds.size
    }

    override fun onBindViewHolder(holder: SortViewHolder, position: Int) {
        holder.sortTypeTitleTextView.text = sortTypeIds[position]
        if (position == selectedSortType) {
            holder.sortTypeTitleTextView.setBackgroundResource(R.drawable.shape_chips_selected)
            holder.sortTypeTitleTextView.setTextColor(
                ContextCompat.getColor(
                    context,
                    R.color.white
                )
            )
        } else {
            holder.sortTypeTitleTextView.setBackgroundResource(R.drawable.shape_chips_unselected)
            holder.sortTypeTitleTextView.setTextColor(
                ContextCompat.getColor(
                    context,
                    R.color.colorAccent
                )
            )
        }
        holder.itemView.setOnClickListener {
            if (holder.adapterPosition != selectedSortType) {
                onSortClickListener.onClicked(holder.adapterPosition)
                notifyItemChanged(selectedSortType)
                selectedSortType = holder.adapterPosition
                notifyItemChanged(selectedSortType)
            }
        }
    }

    interface OnSortClickListener {
        fun onClicked(sortType: Int)
    }

    class SortViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var sortTypeTitleTextView = itemView as TextView
    }
}