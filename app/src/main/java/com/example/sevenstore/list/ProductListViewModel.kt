package com.example.sevenstore.list

import com.example.sevenstore.model.*
import com.example.sevenstore.model.api.*
import com.example.sevenstore.providers.*
import io.reactivex.*

class ProductListViewModel {
    private var apiService: ApiService = ApiServiceProvider.provideApiService()
    fun products(sortType : Int): Single<List<Product>> {
        return apiService.getProducts(sortType)
    }


}