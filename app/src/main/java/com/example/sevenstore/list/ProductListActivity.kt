package com.example.sevenstore.list

import android.os.*
import android.widget.*
import androidx.recyclerview.widget.*
import com.example.sevenstore.*
import com.example.sevenstore.main.*
import com.example.sevenstore.model.*
import com.example.sevenstore.model.api.*
import io.reactivex.android.schedulers.*
import io.reactivex.disposables.*
import io.reactivex.schedulers.*
import kotlinx.android.synthetic.main.activity_product_list_actvity.*

class ProductListActivity : BaseActivity() {
    private var productAdapter: ProductAdapter? = null
    private var sortAdapter: SortAdapter? = null
    override fun getCoordinatorLayoutId(): Int {
        return R.id.cl_list
    }

    companion object {
        const val EXTRA_KEY_SORT = "sort"
        private var sortType: Int? = null
    }

    private var compositeDisposable = CompositeDisposable()
    private var productListViewModel: ProductListViewModel? = null
    private var productObserver = object : SingleSevenStoreObserver<List<Product>>(this) {
        override fun onSuccess(t: List<Product>) {
            productAdapter!!.setProducts(t)
        }

        override fun onSubscribe(d: Disposable) {
            compositeDisposable.add(d)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list_actvity)
        sortType = intent.getIntExtra(EXTRA_KEY_SORT, Product.SORT_LATEST)
        productListViewModel = ProductListViewModel()
        setupViews()
        observe()
    }

    private fun observe() {
        productListViewModel!!.products(sortType!!)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(productObserver)
    }

    private fun setupViews() {
        productAdapter = ProductAdapter()
        rv_list_products.layoutManager = GridLayoutManager(this, 2)
        rv_list_products.adapter = productAdapter

        sortAdapter = SortAdapter(this, sortType!!, object : SortAdapter.OnSortClickListener {
            override fun onClicked(sortType: Int) {
                Toast.makeText(this@ProductListActivity, "" + sortType, Toast.LENGTH_SHORT).show()
                ProductListActivity.sortType = sortType
                observe()
            }

        })
        rv_list_sort.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_list_sort.adapter = sortAdapter


        iv_list_back.setOnClickListener {
            finish()
        }
    }
}
