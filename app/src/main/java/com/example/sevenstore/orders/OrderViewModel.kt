package com.example.sevenstore.orders

import com.example.sevenstore.model.*
import com.example.sevenstore.providers.*
import io.reactivex.*
import io.reactivex.subjects.*

class OrderViewModel {
    private var apiService = ApiServiceProvider.provideApiService()
    private var progressBar = BehaviorSubject.create<Boolean>()

    fun orders(): Single<List<Order>> {
        progressBar.onNext(true)
        return apiService.listOrders().doOnEvent { t1, t2 ->
            progressBar.onNext(false)
        }
    }

    fun getProgressBar(): BehaviorSubject<Boolean> {
        return progressBar
    }
}
