package com.example.sevenstore.orders

import android.os.*
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.*
import com.example.sevenstore.*
import com.example.sevenstore.model.*
import com.example.sevenstore.model.api.*
import io.reactivex.android.schedulers.*
import io.reactivex.disposables.*
import io.reactivex.schedulers.*
import kotlinx.android.synthetic.main.activity_order_history.*

class OrderHistoryActivity : BaseActivity() {
    override fun getCoordinatorLayoutId(): Int {
        return R.id.cl_orderActivity
    }

    private var adapter = OrderAdapter()
    var orderViewModel = OrderViewModel()
    var compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_history)
        setupViews()
        observe()
    }

    private fun observe() {

        val disposable = orderViewModel.getProgressBar()
            .subscribeOn(AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it) prg_order_data.visibility = View.VISIBLE else View.GONE
            }
        compositeDisposable.add(disposable)
        orderViewModel.orders()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleSevenStoreObserver<List<Order>>(this) {
                override fun onSuccess(orders: List<Order>) {
                    rv_orderHistory_orderList.layoutManager = LinearLayoutManager(
                        this@OrderHistoryActivity,
                        LinearLayoutManager.VERTICAL, false
                    )
                    if (orders.isEmpty() || orders == null || orders.size < 1) {
                        Toast.makeText(
                            this@OrderHistoryActivity,
                            "خطا در دریافت اطلاعات",
                            Toast.LENGTH_SHORT
                        ).show()
                        finish()
                    }
                    adapter.setAdapter(orders)
                    rv_orderHistory_orderList.adapter = adapter
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })
    }

    private fun setupViews() {


    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}
