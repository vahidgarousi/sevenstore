package com.example.sevenstore.orders

import android.util.*
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.*
import com.example.sevenstore.*
import com.example.sevenstore.model.*


class OrderAdapter : RecyclerView.Adapter<OrderAdapter.OrderViewHolder>() {

    private var orderList: List<Order>? = null

    fun setAdapter(orderList: List<Order>) {
        this.orderList = orderList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        return OrderViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_order,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return orderList!!.size
    }

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        holder.bindOrder(Order())
    }

    class OrderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvOrderCode = itemView.findViewById<TextView>(R.id.tv_order_code)
        var tvOrderRecipient = itemView.findViewById<TextView>(R.id.tv_order_recipient)
        var tvOrderPayable = itemView.findViewById<TextView>(R.id.tv_order_payable)
        var tvOrderPaymentStatus = itemView.findViewById<TextView>(R.id.tv_order_paymentStatus)
        var tvOrderSate = itemView.findViewById<TextView>(R.id.tv_order_date)

        fun bindOrder(order: Order) {
            if (order == null) {
                Log.i("LOGGER", "null")
            } else {
                Log.i("LOGGER", "is not null")
            }
            /*
            tvOrderCode.text = order.id.toString()
            tvOrderRecipient.text = order.firstName + " " + order.lastName
            tvOrderPayable.text = PriceConverter.convert(order.payable)
            tvOrderPaymentStatus.text = order.paymentStatus;
            tvOrderSate.text = order.date;
             */
        }


    }
}