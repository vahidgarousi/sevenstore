package com.example.sevenstore.utils

import android.content.*
import android.view.*
import android.view.inputmethod.*

object KeyboardUtil {
    fun closeKeyboard(view: View?) {
        if (view != null) {
            val inputMethodManager =
                view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}
