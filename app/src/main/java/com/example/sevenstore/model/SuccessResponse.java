
package com.example.sevenstore.model;

import com.google.gson.annotations.Expose;


public class SuccessResponse {

  @Expose
  private String error;
  @Expose
  private String message;

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

}
