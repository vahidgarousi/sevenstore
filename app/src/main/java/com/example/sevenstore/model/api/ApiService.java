package com.example.sevenstore.model.api;

import com.example.sevenstore.model.AddToCartResponse;
import com.example.sevenstore.model.Banner;
import com.example.sevenstore.model.CartItemCountResponse;
import com.example.sevenstore.model.CartModel;
import com.example.sevenstore.model.CheckoutResponse;
import com.example.sevenstore.model.Comment;
import com.example.sevenstore.model.Order;
import com.example.sevenstore.model.OrderSubmitResponse;
import com.example.sevenstore.model.Product;
import com.example.sevenstore.model.SuccessResponse;
import com.example.sevenstore.model.Token;
import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
  @GET("product/list")
  Single<List<Product>> getProducts(
    @Query("sort") Integer sort
  );

  @GET("banner/slider")
  Single<List<Banner>> getBanners();


  @FormUrlEncoded
  @POST("auth/token")
  Single<Token> getToken(@Field("grant_type") String grantType,
                         @Field("client_id") String clientId,
                         @Field("client_secret") String clientSecret,
                         @Field("username") String username,
                         @Field("password") String password);

  @FormUrlEncoded
  @POST("user/register")
  Single<SuccessResponse> registerUser(@Field("email") String email,
                                       @Field("password") String password);

  @GET("comment/list")
  Single<List<Comment>> getComments(@Query("product_id") int productId);

  @POST("comment/add")
  Single<Comment> addComment(@Body JsonObject jsonObject);


  @GET("cart/list")
  Single<CartModel> getCart();

  @POST("cart/add")
  Single<AddToCartResponse> addToCart(@Body JsonObject jsonObject);


  @POST("cart/changeCount")
  Single<AddToCartResponse> changeCartItemCount(@Body JsonObject jsonObject);


  @POST("cart/remove")
  Single<SuccessResponse> removeCartItem(@Body JsonObject jsonObject);


  @GET("cart/count")
  Single<CartItemCountResponse> getCartItemCount();

  @POST("order/submit")
  Single<OrderSubmitResponse> submitOrder(@Body JsonObject jsonObject);

  @GET("order/checkout")
  Single<CheckoutResponse> checkout(@Query("order_id") String orderId);

  @GET("order/list")
  Single<List<Order>> listOrders();
}
