
package com.example.sevenstore.model;

import com.google.gson.annotations.Expose;

public class Author {

  @Expose
  private String email;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

}
