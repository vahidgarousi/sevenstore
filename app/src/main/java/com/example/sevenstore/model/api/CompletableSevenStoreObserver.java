package com.example.sevenstore.model.api;

import com.example.sevenstore.BaseActivity;
import com.example.sevenstore.exception.ExceptionMessageFactory;

import io.reactivex.CompletableObserver;

abstract public class CompletableSevenStoreObserver implements CompletableObserver {

  private BaseActivity activity;

  public CompletableSevenStoreObserver(BaseActivity activity) {
    this.activity = activity;
  }

  @Override
  public void onError(Throwable e) {
    activity.showSnackBarMessage(ExceptionMessageFactory.getMessage(e));
  }
}
