package com.example.sevenstore.model;

import com.google.gson.annotations.SerializedName;

public class OrderSubmitResponse {
  @SerializedName("order_id")
  private int orderId;

  public int getOrderId() {
    return orderId;
  }

  public void setOrderId(int orderId) {
    this.orderId = orderId;
  }
}
