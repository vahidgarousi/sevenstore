package com.example.sevenstore.model.api;

import com.example.sevenstore.BaseActivity;
import com.example.sevenstore.exception.ExceptionMessageFactory;
import io.reactivex.SingleObserver;

abstract public class SingleSevenStoreObserver<T> implements SingleObserver<T> {

    private BaseActivity activity;

    public SingleSevenStoreObserver(BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onError(Throwable e) {
        activity.showSnackBarMessage(ExceptionMessageFactory.getMessage(e));
    }
}
