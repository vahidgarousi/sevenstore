
package com.example.sevenstore.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class Comment {

  @Expose
  private Author author;
  @Expose
  private String content;
  @Expose
  private String date;
  @Expose
  private Long id;
  @Expose
  private String title;

  public Author getAuthor() {
    return author;
  }

  public void setAuthor(Author author) {
    this.author = author;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

}
