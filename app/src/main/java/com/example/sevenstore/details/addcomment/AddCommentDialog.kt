package com.example.sevenstore.details.addcomment

import android.app.*
import android.os.*
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.example.sevenstore.*
import com.example.sevenstore.model.*
import com.example.sevenstore.model.api.*
import io.reactivex.android.schedulers.*
import io.reactivex.disposables.*
import io.reactivex.schedulers.*

class AddCommentDialog : DialogFragment() {
    lateinit var edtTitle: EditText
    lateinit var edtContent: EditText
    lateinit var btnAddComment: Button
    lateinit var btnCancelComment: Button
    lateinit var progressBar: ProgressBar
    private var compositeDisposable = CompositeDisposable()
    var productId: Int = -1

    private lateinit var addCommentViewModel: AddCommentViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        productId = arguments!!.getInt(PRODUCT_ID)
        if (productId == 0 || productId == -1) {
            dismiss()
        }
        addCommentViewModel = AddCommentViewModel()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!)
        val view = LayoutInflater.from(context!!).inflate(R.layout.dialog_add_comment, null, false)
        edtTitle = view.findViewById<EditText>(R.id.edt_comment_title)
        edtContent = view.findViewById<EditText>(R.id.edt_comment_content)
        btnAddComment = view.findViewById<Button>(R.id.btn_comment_add)
        btnCancelComment = view.findViewById<Button>(R.id.btn_comment_cancel)
        progressBar = view.findViewById<ProgressBar>(R.id.prg_comment_progressBar)

        btnAddComment.setOnClickListener {
            if (edtTitle.text.length > 0 && edtContent.text.length > 0) {
                addCommentViewModel.addComment(
                    productId,
                    edtTitle.text.toString(),
                    edtContent.text.toString()
                ).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object :
                        SingleSevenStoreObserver<Comment>(activity as BaseActivity) {
                        override fun onSuccess(t: Comment) {
                            (activity as BaseActivity).showSnackBarMessage("نظر شما با موفقیت ثبت شد پس از تایید مدیر نمایش داده می شود")
                            dismiss()
                        }

                        override fun onSubscribe(d: Disposable) {
                            compositeDisposable.add(d)
                        }
                    })
            } else {
                if (edtTitle.text.length <= 0) {
                    edtTitle.error = "عنوان نظر نمی تواند خالی باشد"
                }
                if (edtContent.text.length <= 0) {
                    edtContent.error = "متن نظر نمی تواند خالی باشد"
                }
            }
            val disposable = addCommentViewModel.getProgressBarVisibilitySubject()
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    progressBar.visibility = if (it) View.VISIBLE else View.GONE
                    btnAddComment.visibility = if (it) View.INVISIBLE else View.VISIBLE

                }
            compositeDisposable.add(disposable)
        }
        btnCancelComment.setOnClickListener {
            dismiss()
        }
        builder.setView(view)
        return builder.create()
    }

    fun newInstance(productId: Int): AddCommentDialog {
        val args = Bundle()
        args.putSerializable(PRODUCT_ID, productId)
        val fragment = AddCommentDialog()
        fragment.arguments = args
        return fragment
    }

    companion object {
        private val PRODUCT_ID = "myFragment_caught"

    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.dispose()
    }
}