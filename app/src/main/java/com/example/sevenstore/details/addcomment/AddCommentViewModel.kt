package com.example.sevenstore.details.addcomment

import com.example.sevenstore.model.*
import com.example.sevenstore.providers.*
import com.google.gson.*
import io.reactivex.*
import io.reactivex.subjects.*

class AddCommentViewModel {
    private val apiService = ApiServiceProvider.provideApiService()
    private val progressBarVisibilitySubject = BehaviorSubject.create<Boolean>()

    fun addComment(productId: Int, title: String, content: String): Single<Comment> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("title", title)
        jsonObject.addProperty("content", content)
        jsonObject.addProperty("product_id", productId)
        return apiService.addComment(jsonObject)
    }

    fun getProgressBarVisibilitySubject(): BehaviorSubject<Boolean> {
        return progressBarVisibilitySubject
    }
}