package com.example.sevenstore.details

import android.view.*
import android.widget.*
import androidx.recyclerview.widget.*
import com.example.sevenstore.*
import com.example.sevenstore.model.*

class CommentAdapter : RecyclerView.Adapter<CommentAdapter.CommentViewHolder>() {
    var comments = listOf<Comment>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false)
        return CommentViewHolder(view)
    }

    override fun getItemCount(): Int {
        return comments.size
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.bindComment(comments[position])
    }

    fun setCommentList(commentList: List<Comment>) {
        this.comments = commentList
        notifyDataSetChanged()
    }

    class CommentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var titleComment = itemView.findViewById<TextView>(R.id.tv_comment_title)
        var dateComment = itemView.findViewById<TextView>(R.id.tv_comment_date)
        var authorComment = itemView.findViewById<TextView>(R.id.tv_comment_author)
        var contentComment = itemView.findViewById<TextView>(R.id.tv_comment_content)
        fun bindComment(comment: Comment) {
            titleComment.text = comment.title
            dateComment.text = comment.date
            authorComment.text = comment.author.email
            contentComment.text = comment.content
        }
    }
}