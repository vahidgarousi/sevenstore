package com.example.sevenstore.details

import com.example.sevenstore.model.*
import com.example.sevenstore.providers.*
import com.google.gson.*
import io.reactivex.*
import io.reactivex.subjects.*

class ProductDetailsViewModel {
    private val apiService = ApiServiceProvider.provideApiService()
    val progressBarVisibilitySubject = BehaviorSubject.create<Any>()


    fun comments(productId: Int): Single<List<Comment>> {
        progressBarVisibilitySubject.onNext(true)
        return apiService.getComments(productId)
            .doOnEvent { comments, throwable -> progressBarVisibilitySubject.onNext(false) }
    }

    fun addToCart(productId: Int): Single<AddToCartResponse> {
        progressBarVisibilitySubject.onNext(true)
        val jsonObject = JsonObject()
        jsonObject.addProperty("product_id", productId)
        return apiService.addToCart(jsonObject)
            .doOnEvent { comments, throwable -> progressBarVisibilitySubject.onNext(false) }
    }
}
