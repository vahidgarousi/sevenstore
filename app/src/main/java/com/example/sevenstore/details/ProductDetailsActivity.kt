package com.example.sevenstore.details

import android.graphics.*
import android.os.*
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.*
import com.example.sevenstore.*
import com.example.sevenstore.R
import com.example.sevenstore.cart.*
import com.example.sevenstore.details.addcomment.*
import com.example.sevenstore.model.*
import com.example.sevenstore.model.api.*
import com.example.sevenstore.utils.*
import com.squareup.picasso.*
import io.reactivex.android.schedulers.*
import io.reactivex.disposables.*
import io.reactivex.functions.*
import io.reactivex.schedulers.*
import kotlinx.android.synthetic.main.activity_product_details.*
import org.greenrobot.eventbus.*

class ProductDetailsActivity : BaseActivity() {

    override fun getCoordinatorLayoutId(): Int {
        return R.id.cl_details
    }

    lateinit var product: Product
    var viewModel: ProductDetailsViewModel = ProductDetailsViewModel()
    var commentAdapter = CommentAdapter()
    var compositeDisposable = CompositeDisposable()

    companion object {
        const val EXTRA_KEY_PRODUCT = "product"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        product = intent.getParcelableExtra<Product>(EXTRA_KEY_PRODUCT)
        if (product == null) {
            finish()
        }
        setContentView(R.layout.activity_product_details)
        viewModel.comments(product.id)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleSevenStoreObserver<List<Comment>>(this) {
                override fun onSuccess(commentList: List<Comment>) {
                    rv_details_comments.layoutManager = LinearLayoutManager(
                        this@ProductDetailsActivity,
                        RecyclerView.VERTICAL,
                        false
                    )
                    commentAdapter.setCommentList(commentList)
                    rv_details_comments.adapter = commentAdapter
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })

        val disposable =
            viewModel.progressBarVisibilitySubject.subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(Consumer {
                    prg_details_comment.visibility = if (it as Boolean) View.VISIBLE else View.GONE
                })
        compositeDisposable.add(disposable)
        setupViews()
    }

    private fun setupViews() {
        Picasso.get().load(product.image).into(iv_details_image)
        tv_details_price.text = PriceConverter.convert(product.price)
        tv_details_prevPrice.paintFlags =
            tv_details_prevPrice.paintFlags.or(Paint.STRIKE_THRU_TEXT_FLAG)
        tv_details_prevPrice.text = PriceConverter.convert(product.previousPrice)
        tv_details_title.text = product.title
        tv_details_activityTitle.text = product.title
        tv_details_addComment.setOnClickListener {
            val addCommentDialog = AddCommentDialog().newInstance(product.id)
            addCommentDialog.show(supportFragmentManager, null)
        }
        btn_details_addToCart.setOnClickListener {

            viewModel.addToCart(product.id).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleSevenStoreObserver<AddToCartResponse>(this) {
                    override fun onSuccess(t: AddToCartResponse) {
                        prg_details_addToCartProgressBar.visibility = View.GONE
                        it.visibility = View.VISIBLE
                        Toast.makeText(
                            this@ProductDetailsActivity,
                            "با موفقیت به سبد خرید شما اضافه شد",
                            Toast.LENGTH_LONG
                        ).show()
                        var count = CartItemCountContainer.getCount()
                        count += 1
                        CartItemCountContainer.updateCount(count)
                        EventBus.getDefault().post(object : OnCartItemCountChanged(count) {

                        })
                    }

                    override fun onSubscribe(d: Disposable) {
                        compositeDisposable.add(d)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        prg_details_addToCartProgressBar.visibility = View.GONE
                        it.visibility = View.VISIBLE
                    }

                })
            prg_details_addToCartProgressBar.visibility = View.VISIBLE
            it.visibility = View.INVISIBLE
        }
        iv_details_back.setOnClickListener {
            finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}
