package com.example.sevenstore.auth

import android.content.*
import com.example.sevenstore.model.*
import com.example.sevenstore.providers.*
import io.reactivex.*
import io.reactivex.subjects.*

class AuthenticationViewModel {
    private var apiService = ApiServiceProvider.provideApiService()
    var isInLoginMode: BehaviorSubject<Boolean> = BehaviorSubject.create()
    var progressBarVisibilitySubject: BehaviorSubject<Boolean> = BehaviorSubject.create()

    fun authentication(context: Context, email: String, password: String): Completable {
        progressBarVisibilitySubject.onNext(true)
        var singleToken: Single<Token>? = null
        if (isInLoginMode.value == null || isInLoginMode.value!!) {
            singleToken = apiService.getToken(
                "password",
                "2",
                "kyj1c9sVcksqGU4scMX7nLDalkjp2WoqQEf8PKAC",
                email,
                password
            )
        } else {
            singleToken = apiService.registerUser(email, password)
                .flatMap {
                    apiService.getToken(
                        "password",
                        "2",
                        "kyj1c9sVcksqGU4scMX7nLDalkjp2WoqQEf8PKAC",
                        email,
                        password
                    )
                }
        }
        val userInfoManager = UserInfoManager(context)
        return singleToken!!.doOnSuccess { token ->
            if (token != null) {
                userInfoManager.saveToken(token.accessToken, token.refreshToken)
                userInfoManager.saveEmail(email)
                TokenContainer.updateToken(token.accessToken)
            }
        }.doOnEvent { token, throwable ->
            progressBarVisibilitySubject.onNext(false)
        }.toCompletable()
    }

    fun OnAuthenticationClicked() {
        isInLoginMode.onNext(isInLoginMode.value != null && !isInLoginMode.value!!)

    }

    fun getIsInLoginMode(): BehaviorSubject<Boolean> {
        return isInLoginMode
    }

    fun getProgressBarVisibility(): BehaviorSubject<Boolean> {
        return progressBarVisibilitySubject
    }
}