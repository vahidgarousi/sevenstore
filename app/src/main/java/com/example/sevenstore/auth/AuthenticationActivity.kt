package com.example.sevenstore.auth

import android.os.*
import android.view.*
import android.widget.*
import com.example.sevenstore.*
import com.example.sevenstore.model.api.*
import com.example.sevenstore.utils.*
import io.reactivex.android.schedulers.*
import io.reactivex.disposables.*
import io.reactivex.schedulers.*
import kotlinx.android.synthetic.main.activity_authentication.*
import org.greenrobot.eventbus.*

class AuthenticationActivity : BaseActivity() {
    private var authenticationViewModel: AuthenticationViewModel? = null
    private var compositeDisposable = CompositeDisposable()
    override

    fun getCoordinatorLayoutId(): Int {
        return R.id.rl_auth_main
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
        authenticationViewModel = AuthenticationViewModel()
        setupViews()
        observe()
    }

    private fun observe() {
        val loginModeDisposable = authenticationViewModel!!.isInLoginMode
            .subscribeOn(AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { isInLoginMode ->
                if (isInLoginMode) {
                    tv_auth_changeAuthenticationMode.text = "ثبت نام"
                    btn_auth_authentication.text = "ورود به حساب کاربری"
                } else {
                    tv_auth_changeAuthenticationMode.text = "ورود به حساب کاربری"
                    btn_auth_authentication.text = "ثبت نام"
                }
            }
        val progressBarDisposable = authenticationViewModel!!.progressBarVisibilitySubject
            .subscribeOn(AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { shouldShowProgressBar ->
                if (shouldShowProgressBar) {
                    ln_progressBar.visibility = View.VISIBLE
                } else {
                    ln_progressBar.visibility = View.GONE
                }
            }
        compositeDisposable.add(loginModeDisposable)
        compositeDisposable.add(progressBarDisposable)

    }

    private fun setupViews() {
        tv_auth_changeAuthenticationMode.setOnClickListener {
            authenticationViewModel!!.OnAuthenticationClicked()
        }

        iv_auth_back.setOnClickListener {
            finish()
        }
        btn_auth_authentication.setOnClickListener {
            KeyboardUtil.closeKeyboard(currentFocus)
            authenticationViewModel!!.authentication(
                this,
                edt_auth_email.text.toString(),
                edt_auth_password.text.toString()
            ).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : CompletableSevenStoreObserver(this) {
                    override fun onComplete() {
                        EventBus.getDefault().post(object : OnUserAuthenticate() {})
                        Toast.makeText(
                            this@AuthenticationActivity,
                            "شما با موفقیت وارد شدید",
                            Toast.LENGTH_LONG
                        ).show()
                        finish()
                    }

                    override fun onSubscribe(d: Disposable) {
                        compositeDisposable.add(d)
                    }

                })
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()

    }

}
