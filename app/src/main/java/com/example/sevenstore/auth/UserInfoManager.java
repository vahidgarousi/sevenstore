package com.example.sevenstore.auth;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.Nullable;

public class UserInfoManager {
  private SharedPreferences sharedPreferences;

  public UserInfoManager(Context context) {
    this.sharedPreferences = context
      .getSharedPreferences("user info", Context.MODE_PRIVATE);
  }


  public void saveEmail(String email) {
    SharedPreferences.Editor edit = sharedPreferences.edit();
    edit.putString("email", email);
    edit.apply();
  }

  public String getEmail() {
    return sharedPreferences.getString("email", "");
  }
  public void saveToken(String token, String refreshToken) {
    SharedPreferences.Editor edit = sharedPreferences.edit();
    edit.putString("token", token);
    edit.putString("refresh_token", refreshToken);
    edit.apply();
  }

  @Nullable
  public String getToken() {
    return sharedPreferences.getString("token", null);
  }

  @Nullable
  public String refreshToken() {
    return sharedPreferences.getString("refresh_token", null);
  }

  public void clear() {
    sharedPreferences.edit().clear().apply();
  }
}
