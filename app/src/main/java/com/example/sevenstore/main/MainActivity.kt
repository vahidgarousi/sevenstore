package com.example.sevenstore.main

import android.content.*
import android.graphics.*
import android.os.*
import android.view.*
import android.widget.*
import androidx.core.content.*
import androidx.core.content.res.*
import androidx.recyclerview.widget.*
import com.example.sevenstore.*
import com.example.sevenstore.R
import com.example.sevenstore.auth.*
import com.example.sevenstore.cart.*
import com.example.sevenstore.list.*
import com.example.sevenstore.model.*
import com.example.sevenstore.model.api.*
import com.example.sevenstore.orders.*
import com.mikepenz.materialdrawer.*
import com.mikepenz.materialdrawer.holder.*
import com.mikepenz.materialdrawer.model.*
import com.mikepenz.materialdrawer.model.interfaces.*
import io.reactivex.android.schedulers.*
import io.reactivex.disposables.*
import io.reactivex.schedulers.*
import kotlinx.android.synthetic.main.activity_main.*
import org.greenrobot.eventbus.*

class MainActivity : BaseActivity(), View.OnClickListener {
    private var compositeDisposable = CompositeDisposable()
    private var latestProductAdapter: ProductAdapter? = null
    private var popularProductAdapter: ProductAdapter? = null
    private var bannerAdapter: BannerAdapter? = null
    private var drawer: Drawer? = null
    private lateinit var userInfoManager: UserInfoManager
    private lateinit var authDrawerItem: PrimaryDrawerItem
    private lateinit var cartDrawerItem: PrimaryDrawerItem
    private lateinit var profileDrawerItem: ProfileDrawerItem
    private lateinit var accountHeader: AccountHeader
    private lateinit var drawerFont: Typeface
    override fun onClick(v: View?) {
        val intent = Intent(this@MainActivity, ProductListActivity::class.java)
        when (v!!.id) {
            R.id.tv_main_viewAllLatestProducts -> {
                intent.putExtra(ProductListActivity.EXTRA_KEY_SORT, Product.SORT_LATEST)
            }
            R.id.tv_main_viewAllPopularProducts -> {
                intent.putExtra(ProductListActivity.EXTRA_KEY_SORT, Product.SORT_POPULAR)
            }
        }
        startActivity(intent)

    }


    override fun getCoordinatorLayoutId(): Int {
        return R.id.cl_main
    }

    private lateinit var mainViewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        userInfoManager = UserInfoManager(this)
        mainViewModel = MainViewModel()
        setupViews()
        observe()
        iv_main_cart.setOnClickListener {
            startActivity(Intent(this@MainActivity, CartActivity::class.java))
        }
        iv_main_menu.setOnClickListener {
            drawer?.openDrawer()
        }
        drawerFont = ResourcesCompat.getFont(this@MainActivity, R.font.iran_sans)!!
        setupDrawer()
    }

    private fun setupDrawer() {
        updateProfileDrawerItem()
        updateAccountHeader()
        cartDrawerItem = PrimaryDrawerItem()
            .withName("سبد خرید")
            .withTypeface(drawerFont)
            .withBadge("1")
            .withOnDrawerItemClickListener { view, position, drawerItem ->
                if (TokenContainer.getToken() == null) {
                    Toast.makeText(
                        this@MainActivity,
                        "ابتدا باید وارد حساب کاربری شوید",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    startActivity(
                        Intent(
                            this@MainActivity,
                            CartActivity::class.java
                        )
                    )
                }
                return@withOnDrawerItemClickListener true
            }
            .withBadgeStyle(
                BadgeStyle()
                    .withBadgeBackground(
                        ContextCompat.getDrawable(
                            this@MainActivity,
                            R.drawable.shape_badge
                        )
                    )
                    .withTextColor(ContextCompat.getColor(this@MainActivity, R.color.white))
            )
            .withIdentifier(ID_CART)
            .withSelectable(false)
        val orderHistoryDrawerDrawerItem = PrimaryDrawerItem()
            .withName("سوابق سفارش")
            .withIdentifier(ID_ORDER_HISTORY)
            .withOnDrawerItemClickListener { view, position, drawerItem ->
                if (TokenContainer.getToken() == null) {
                    Toast.makeText(
                        this@MainActivity,
                        "ابتدا وارد حساب کاربری خود شوید",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    startActivity(Intent(this@MainActivity, OrderHistoryActivity::class.java))
                }
                return@withOnDrawerItemClickListener true
            }
            .withTypeface(drawerFont)
            .withSelectable(false)

        updateAuthDrawerItem()
        drawer = DrawerBuilder()
            .withActivity(this)
            .withDrawerGravity(Gravity.RIGHT)
            .withSelectedItem(-1)
            .addDrawerItems(cartDrawerItem, orderHistoryDrawerDrawerItem, authDrawerItem)
            .withAccountHeader(accountHeader)
            .build()

    }

    private fun updateProfileDrawerItem() {
        val name = when {
            TokenContainer.getToken() != null -> ""
            else -> "کاربر مهمان"
        }
        val email =
            when {
                TokenContainer.getToken() != null -> userInfoManager.email
                else -> "ورود به حساب کاربری یا ثبت نام"
            }
        if (drawer == null) {
            profileDrawerItem = ProfileDrawerItem()
                .withName(name)
                .withEmail(email)
                .withTypeface(drawerFont)
                .withIcon(R.drawable.profile_img)
        } else {
            profileDrawerItem
                .withName(name)
                .withEmail(email)
            accountHeader.updateProfile(profileDrawerItem)
        }
    }

    private fun updateAuthDrawerItem() {
        val authTitle = if (TokenContainer.getToken() != null) {
            "خروج از حساب کاربری"
        } else {
            "ورود به حساب کاربری"
        }
        if (drawer == null) {
            authDrawerItem = PrimaryDrawerItem()
                .withName(authTitle)
                .withIdentifier(ID_AUTH)
                .withTypeface(drawerFont)
                .withOnDrawerItemClickListener(object : Drawer.OnDrawerItemClickListener {
                    override fun onItemClick(
                        view: View?,
                        position: Int,
                        drawerItem: IDrawerItem<*, *>?
                    ): Boolean {
                        if (TokenContainer.getToken() != null) {
                            userInfoManager.clear()
                            TokenContainer.updateToken(null)
                            updateAuthDrawerItem()
                            updateProfileDrawerItem()
                            CartItemCountContainer.updateCount(0)
                            EventBus.getDefault().post(object : OnCartItemCountChanged(0) {
                            })
                        } else {
                            startActivity(
                                Intent(
                                    this@MainActivity,
                                    AuthenticationActivity::class.java
                                )
                            )
                        }
                        return true
                    }

                })
                .withSelectable(false)
        } else {
            authDrawerItem.withName(authTitle)
            drawer!!.updateItem(authDrawerItem)
        }

    }

    private fun updateAccountHeader() {
        updateProfileDrawerItem()
        accountHeader = AccountHeaderBuilder()
            .addProfiles(
                profileDrawerItem
            ).withOnAccountHeaderListener { view, profile, current ->
                if (TokenContainer.getToken() == null) {
                    startActivity(Intent(this@MainActivity, AuthenticationActivity::class.java))
                }
                return@withOnAccountHeaderListener false
            }
            .withHeaderBackground(
                ContextCompat.getDrawable(
                    this@MainActivity,
                    R.color.colorPrimary
                )
            )
            .withTypeface(drawerFont)
            .withActivity(this@MainActivity)
            .build()
    }

    private fun observe() {
        mainViewModel.latestProducts()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleSevenStoreObserver<List<Product>>(this) {
                override fun onSuccess(latestProducts: List<Product>) {
                    latestProductAdapter!!.setProducts(latestProducts)
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })
        mainViewModel.popularProduct()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleSevenStoreObserver<List<Product>>(this) {
                override fun onSuccess(latestProducts: List<Product>) {
                    popularProductAdapter!!.setProducts(latestProducts)
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })
        mainViewModel.banners()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleSevenStoreObserver<List<Banner>>(this) {
                override fun onSuccess(banners: List<Banner>) {
                    bannerAdapter!!.setBanners(banners)
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })
        getUserCartItemCount()
    }

    private fun setupViews() {
        latestProductAdapter = ProductAdapter()
        rv_main_latestProducts.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, true)
        rv_main_latestProducts.adapter = latestProductAdapter

        popularProductAdapter = ProductAdapter()
        rv_main_popularProducts.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, true)
        rv_main_popularProducts.adapter = popularProductAdapter

        bannerAdapter = BannerAdapter()
        rv_main_slider.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, true)
        rv_main_slider.adapter = bannerAdapter
        val snapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(rv_main_slider)

    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    companion object {
        private const val ID_CART: Long = 1
        private const val ID_ORDER_HISTORY: Long = 2
        private const val ID_AUTH: Long = 3
    }


    override fun onResume() {
        super.onResume()
        updateAuthDrawerItem()
        updateProfileDrawerItem()
        updateCartItemCountBadge(CartItemCountContainer.getCount())
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onCartItemCountChanged(onCartItemCountChanged: OnCartItemCountChanged) {
        updateCartItemCountBadge(onCartItemCountChanged.count)
    }

    private fun updateCartItemCountBadge(count: Int) {
        if (count > 0) {
            tv_main_cartItemCountBadge.visibility = View.VISIBLE
            tv_main_cartItemCountBadge.text = count.toString()

        } else {
            tv_main_cartItemCountBadge.visibility = View.GONE
        }

        cartDrawerItem.withBadge("" + count)
        drawer!!.updateItem(cartDrawerItem)
    }


    private fun getUserCartItemCount() {
        mainViewModel.cartItemCount
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleSevenStoreObserver<CartItemCountResponse>(this) {
                override fun onSuccess(cartItemCountResponse: CartItemCountResponse) {
                    CartItemCountContainer.updateCount(cartItemCountResponse.count)
                    EventBus.getDefault().post(OnCartItemCountChanged(cartItemCountResponse.count))
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onUserAuthenticate(onUserAuthenticate: OnUserAuthenticate) {
        getUserCartItemCount()
    }
}
