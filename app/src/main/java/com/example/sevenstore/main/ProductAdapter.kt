package com.example.sevenstore.main

import android.content.*
import android.graphics.*
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.*
import com.example.sevenstore.R
import com.example.sevenstore.details.*
import com.example.sevenstore.model.*
import com.example.sevenstore.utils.*
import com.squareup.picasso.*

class ProductAdapter : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    private var products = listOf<Product>()

    fun setProducts(products: List<Product>) {
        this.products = products
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        return ProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bindProduct(products[position])
    }

    override fun getItemCount(): Int {
        return products.size
    }

    class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var productImage = itemView.findViewById<ImageView>(R.id.iv_product_image)
        var productPrePrice = itemView.findViewById<TextView>(R.id.tv_product_prevPrice)
        var productCurrentPrice = itemView.findViewById<TextView>(R.id.tv_product_currentPrice)
        var productTitle = itemView.findViewById<TextView>(R.id.tv_product_title)
        fun bindProduct(product: Product) {
            Picasso.get().load(product.image).into(productImage)
            productTitle.text = product.title
            productPrePrice.paintFlags = productPrePrice.paintFlags.or(Paint.STRIKE_THRU_TEXT_FLAG)
            productPrePrice.text = PriceConverter.convert(product.previousPrice)
            productCurrentPrice.text = PriceConverter.convert(product.price)
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, ProductDetailsActivity::class.java)
                intent.putExtra(ProductDetailsActivity.EXTRA_KEY_PRODUCT, product)
                itemView.context.startActivity(intent)
            }
        }
    }
}
