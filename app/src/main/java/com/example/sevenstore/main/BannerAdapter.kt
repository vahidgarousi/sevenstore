package com.example.sevenstore.main

import android.view.*
import android.widget.*
import androidx.recyclerview.widget.*
import com.example.sevenstore.model.*
import com.squareup.picasso.*

class BannerAdapter : RecyclerView.Adapter<BannerAdapter.BannerViewHolder>() {
    private var banners = arrayListOf<Banner>()
    fun setBanners(banners: List<Banner>) {
        this.banners = banners as ArrayList<Banner>
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BannerViewHolder {
        val imageView = ImageView(parent.context)
        imageView.layoutParams = ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        imageView.adjustViewBounds = true
        return BannerViewHolder(imageView)
    }

    override fun getItemCount(): Int {
        return banners.size
    }

    override fun onBindViewHolder(holder: BannerViewHolder, position: Int) {
        Picasso.get().load(banners[position].image).into(holder.imageView)
    }

    class BannerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView = itemView as ImageView
    }
}