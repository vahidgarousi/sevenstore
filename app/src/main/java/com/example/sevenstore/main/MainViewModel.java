package com.example.sevenstore.main;

import com.example.sevenstore.model.Banner;
import com.example.sevenstore.model.CartItemCountResponse;
import com.example.sevenstore.model.Product;
import com.example.sevenstore.model.api.ApiService;
import com.example.sevenstore.providers.ApiServiceProvider;

import java.util.List;

import io.reactivex.Single;

public class MainViewModel {
  private ApiService apiService = ApiServiceProvider.provideApiService();

  public Single<List<Product>> latestProducts() {
    return apiService.getProducts(null);
  }

  public Single<List<Product>> popularProduct() {
    return apiService.getProducts(Product.SORT_POPULAR);
  }

  public Single<List<Product>> highToLowProduct() {
    return apiService.getProducts(Product.SORT_PRICE_HIGH_TO_LOW);
  }

  public Single<List<Product>> lowToHighProduct() {
    return apiService.getProducts(Product.SORT_LOW_TO_HIGH);
  }


  public Single<List<Banner>> banners() {
    return apiService.getBanners();
  }

  public Single<CartItemCountResponse> getCartItemCount() {
    return apiService.getCartItemCount();
  }
}
